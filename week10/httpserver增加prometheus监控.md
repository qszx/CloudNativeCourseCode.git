## 按课程内容安装完Harbor

`kubectl get svc -n harbor`

```
NAME                   TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                                     AGE
harbor                 NodePort    10.110.187.10    <none>        80:30002/TCP,443:30003/TCP,4443:30004/TCP   3d3h
```

##### 配置hosts文件，域名core.harbor.domain指向harbor的service的ClusterIp

`cat /etc/hosts`

```
10.110.187.10 core.harbor.domain
```

## 上传httpserver镜像

##### 构建镜像

`docker build -t core.harbor.domain/homework/httpserver:v3.0 .`

##### 上传镜像到Harbor

`docker push core.harbor.domain/homework/httpserver:v3.0`

![image-20220311211633992](https://gitee.com/qszx/image/raw/master/image-20220311211633992.png)



##### 问题：导入github.com/cncamp/golang/httpserver/metrics依赖包报错

##### 分析：go mod 命令下载github的依赖包默认是master分支，但是孟老师的代码在metrics分支

##### 解决方法：在命令源码文件目录执行 go get github.com/cncamp/golang/httpserver@metrics命令，可以下载不是master分支的代码，然后再执行git mod tidy编译器未报错了

## 部署httpserver

##### deployment.yaml添加annotations:

```yaml
annotations:
  prometheus.io/scrape: "true"
  prometheus.io/port: "80"
```

##### 容器指定ports：

```yaml
containerPort: 80
```

##### 创建应用:

 `kubectl  apply -f deployment.yaml`

##### 查看应用IP:

 `kubectl get pod -owide`

![image-20220311233604234](https://gitee.com/qszx/image/raw/master/image-20220311233604234.png)

##### 访问应用（多点几次，收集数据）：

`curl 192.168.145.225/hello`

![image-20220311233750893](https://gitee.com/qszx/image/raw/master/image-20220311233750893.png)

##### 修改prometheus的service为NodePort类型

![image-20220311234032837](https://gitee.com/qszx/image/raw/master/image-20220311234032837.png)

##  Promethus 界面中查询延时指标数据

##### 在浏览器根据虚拟机IP和映射的端口号访问prometheus页面

##### 在查找框输入`httpserver_execution_latency_seconds_bucket`，执行查看结果：

![image-20220311235653100](https://gitee.com/qszx/image/raw/master/image-20220311235653100.png)

###### 在查找框输入histogram_quantile(0.95,sum(rate(httpserver_execution_latency_seconds_bucket[5m]))by (le))，执行查看结果：

![image-20220311234505820](https://gitee.com/qszx/image/raw/master/image-20220311234505820.png)



##### 问题1：创建pod的时候无法拉取harbor私服镜像

##### 分析：本地的k8s用的containerd，但是harbor是用的docker

##### 解决方法：

```shell
vi /etc/containerd/config.toml
```

添加以下内容：

```toml
[plugins."io.containerd.grpc.v1.cri".registry]
   [plugins."io.containerd.grpc.v1.cri".registry.mirrors]
       [plugins."io.containerd.grpc.v1.cri".registry.mirrors."docker.io"]
          		endpoint = ["https://registry-1.docker.io"]
       [plugins."io.containerd.grpc.v1.cri".registry.mirrors."core.harbor.domain"]
         		endpoint = ["https://core.harbor.domain"]
   [plugins."io.containerd.grpc.v1.cri".registry.configs]
   		 [plugins."io.containerd.grpc.v1.cri".registry.configs."core.harbor.domain".tls]
          		insecure_skip_verify = true
       	 [plugins."io.containerd.grpc.v1.cri".registry.configs."core.harbor.domain".auth]
          		username = "admin"
          		password = "Harbor12345"
```



##### 问题2：启动pod的时候报错，open /tmp/xxxxx no such file or directory

##### 分析：glog包默认会把日志打印到/tmp目录下，dockerfile使用的多阶段构建，最后一阶段用的scratch镜像，没有/tmp目录

##### 解决方法：用alpine镜像替换scratch镜像

## Grafana Dashboard 展现延时分配情况

##### 在grafana导入httpserver-latency.json中的内容，然后查看：

![image-20220311235726676](https://gitee.com/qszx/image/raw/master/image-20220311235726676.png)
