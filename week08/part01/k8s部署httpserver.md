## 一、修改httpserver的源码

##### 增加根据环境变量获取端口号

```
port := os.Getenv("httpport")
```

##### 增加优雅停止

```
sig := make(chan os.Signal, 1)
signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
<-sig

glog.Info("Http server shutting down...")
ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
defer cancel()

if err := server.Shutdown(ctx); err != nil {
   glog.Info("error occured when shutting down http server:", err)
}
glog.Info("shutting down http server successfully")
```

##### 重新构建镜像

`docker build -t eulerformula/homework:v2.0 .`

##### 登录

`docker login --username=eulerformula`

##### 推送镜像

`docker push eulerformula/homework:v2.0`

## 二、根据配置文件httpserver.properties创建ConfigMap

##### httpserver.properties内容如下

`httpport=8801`

##### 创建ConfigMap

`kubectl create configmap httsesrver-config --from-env-file=httpserver.properties`

##### 查看ConfigMap

`kubectl get configmap httsesrver-config -oyaml` 输出如下

```
apiVersion: v1
data:
  httpport: "8801"
kind: ConfigMap
metadata:
  creationTimestamp: "2022-02-27T15:23:10Z"
  name: httsesrver-config
  namespace: default
  resourceVersion: "1136653"
  uid: b7928c4e-9d19-4e52-83a7-9d3d4400eb54
```

## 三、创建应用

##### 创建pod： `kubectl apply -f httpserver.yaml`

##### 查看pod

![httpserver1](https://gitee.com/qszx/image/raw/master/httpserver1.PNG)

#####  查看pod的事件：`kubectl describe pod http-server`

![httpserver2](https://gitee.com/qszx/image/raw/master/httpserver2.PNG)

##### 删除pod：`kubectl delete pod http-server`

##### 查看pod的日志：`kubectl logs -f http-server`，实现了优雅终止

![httpserver3](https://gitee.com/qszx/image/raw/master/httpserver3.PNG)

