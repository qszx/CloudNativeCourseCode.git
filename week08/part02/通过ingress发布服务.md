## 启动httpserver应用

 `kubectl apply -f httpserver.yaml`

![image-20220306115427341](https://gitee.com/qszx/image/raw/master/image-20220306115427341.png)

## 创建service对象

`kubectl apply -f service.yaml`

![image-20220306115711575](https://gitee.com/qszx/image/raw/master/image-20220306115711575.png)

`kubectl get ep`

![image-20220306115816607](https://gitee.com/qszx/image/raw/master/image-20220306115816607.png)

## 创建ingress-nginx-controller

##### 问题：k8s.gcr.io下的镜像国内无法访问

##### 解决方法：在docker hub搜索对应的镜像，然后替换

```
image: liangjw/ingress-nginx-controller:v1.1.1
```

```
image: liangjw/kube-webhook-certgen:v1.1.1
```





`kubectl apply -f nginx-ingress-deployment.yaml`

`kubectl get svc -n ingress-nginx`

![image-20220306120123087](https://gitee.com/qszx/image/raw/master/image-20220306120123087.png)

## 配置证书

##### 生成证书和私钥

```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout tls.key -out tls.crt -subj "/CN=cncamp.com/O=cncamp" -addext "subjectAltName = DNS:cncamp.com"
```

##### 创建secret对象

```
kubectl create secret tls cncamp-tls --cert=./tls.crt --key=./tls.key
```

## 创建ingress对象，配置路由规则

`kubectl apply -f ingress.yaml`

![image-20220306121021108](https://gitee.com/qszx/image/raw/master/image-20220306121021108.png)

## 检验结果

```
curl -H "Host: cncamp.com" https://10.110.50.109 -v -k
```

##### 多次执行命令，查看pod日志，会看到两个pod交替打印日志

`kubectl logs -f httpserver-deployment-7d9cb7f547-jgpt6`

![image-20220306121408866](https://gitee.com/qszx/image/raw/master/image-20220306121408866.png)

 `kubectl logs -f httpserver-deployment-7d9cb7f547-h47fg`

![image-20220306121535174](https://gitee.com/qszx/image/raw/master/image-20220306121535174.png)