## 一、发布httpserver镜像

###### 编写service0和service1两个httpserver程序，在service0接收请求后会向service1发送请求，同时要收集传入请求中的以下标头并将其传播到任何传出请求：

```
x-request-id
x-b3-traceid
x-b3-spanid
x-b3-parentspanid
x-b3-sampled
x-b3-flags
x-ot-span-context
```

###### 发布service0镜像：

```
docker build -t eulerformula/service0:v1.0 .
docker push eulerformula/service0:v1.0
```

发布service1镜像：

```
docker build -t eulerformula/service1:v1.0 .

docker push eulerformula/service1:v1.0 
```

###### 查看dockerhub镜像仓库：

![image-20220325220843795](https://gitee.com/qszx/image/raw/master/image-20220325220843795.png)

## 二、在Kubernetes集群中部署service0和service1

###### 创建命名空间tracing，添加label让istio自动注入：

```
kubectl create ns tracing
kubectl label ns tracing istio-injection=enabled
```

###### 创建service：

```
kubectl -n tracing apply -f service0.yaml
kubectl -n tracing apply -f service1.yaml
```

![service](https://gitee.com/qszx/image/raw/master/service.png)

## 三、通过istio暴露service

###### VirtualService中配置路由，`/http/service0`重定向到`/service0`：

```
http:
  - match:
      - uri:
          exact: "/http/service0"
    rewrite:
      uri: "/service0"
    route:
      - destination:
          host: service0
          port:
            number: 80
```

###### 创建TLS证书和私钥：

```sh
openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -subj '/O=cncamp Inc./CN=*.cncamp.io' -keyout cncamp.io.key -out cncamp.io.crt
```

###### 创建secret：

```sh
kubectl create -n istio-system secret tls cncamp-credential --key=cncamp.io.key --cert=cncamp.io.crt
```

###### Gateway中以https方式暴露服务：

```
apiVersion: networking.istio.io/v1beta1
kind: Gateway
metadata:
  name: service0
spec:
  selector:
    istio: ingressgateway
  servers:
    - hosts:
        - httpsserver.cncamp.io
      port:
        name: http-service0
        number: 443
        protocol: HTTPS
      tls:
        mode: SIMPLE
        credentialName: cncamp-credential
```

###### 创建VirtualService和Gateway：

```
kubectl apply -f istio-specs.yaml -n tracing
```

![image-20220325221807880](https://gitee.com/qszx/image/raw/master/image-20220325221807880.png)

###### 访问service0：

```
curl --resolve httpsserver.cncamp.io:443:10.104.76.93 https://httpsserver.cncamp.io/http/service0 -v -k
```

![image-20220325221955749](https://gitee.com/qszx/image/raw/master/image-20220325221955749.png)

## 四、接入jaeger进行服务追踪

###### 创建jaeger:

```
kubectl apply -f jaeger.yaml
```

###### 访问服务100次进行采样：

```shell
for i in $(seq 1 100); do curl --resolve httpsserver.cncamp.io:443:10.104.76.93 -s  -k -o /dev/null "https://httpsserver.cncamp.io/http/service0"; done
```

###### 修改tracing service类型为NodePort，可以从浏览器访问Jaeger:

 ![image-20220325223643569](https://gitee.com/qszx/image/raw/master/image-20220325223643569.png)

###### 打开浏览器访问http://192.168.104.2:31728：

![image-20220325223948923](https://gitee.com/qszx/image/raw/master/image-20220325223948923.png)

![image-20220325224043743](https://gitee.com/qszx/image/raw/master/image-20220325224043743.png)
