package main

import "fmt"

func main() {
	strArr := [5]string{"I", "am", "stupid", "and", "weak"}
	for index, value := range strArr {
		if value == "stupid" {
			strArr[index] = "smart"
		} else if value == "weak" {
			strArr[index] = "strong"

		}
	}

	fmt.Println(strArr)
}
